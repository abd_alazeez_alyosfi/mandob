import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mandob/admin/sales.dart';
import 'package:mandob/admin/satte/sales_cubit.dart';

import '../shared/app_text_field.dart';
import '../shared/shared_widget.dart';
import '../shared/stateful_dropdown.dart';
import 'models/sale_person.dart';

class EditSalesPerson extends StatefulWidget {
  const EditSalesPerson(
      {Key? key, required this.salePersonModel, required this.docId})
      : super(key: key);
  final SalePersonModel salePersonModel;
  final String docId;

  @override
  State<EditSalesPerson> createState() => _EditSalesPersonState();
}

class _EditSalesPersonState extends State<EditSalesPerson> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  File? profileImage;
  var picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController phoneController = TextEditingController();
    nameController.text = widget.salePersonModel.name;
    phoneController.text = widget.salePersonModel.phone;

    String region = widget.salePersonModel.region;
    return BlocConsumer<SalesPersonCubit, SalesPersonState>(
      listener: (context, state) {
        if (state is UpdateSalesPersonLoaded) {
          Navigator.pop(context);

        }
      },
      builder: (context, state) {
        var cubit = SalesPersonCubit.get(context);
        return Scaffold(
          appBar: AppBar(
            title: const Text('Edit Sales Person Details',
                style: TextStyle(fontWeight: FontWeight.bold)),
            centerTitle: true,
          ),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 36,
                  ),
                  circleAvatar(context,
                      profileImage: null,
                      image: widget.salePersonModel.image, onTap: () async {
                    final XFile? pickedFile = await picker.pickImage(
                      source: ImageSource.gallery,
                    );
                    if (pickedFile != null) {
                      profileImage = File(pickedFile.path);
                    } else {
                      print('no image selected');
                    }
                    setState(() {});
                  }),
                  AppTextField(
                    hintText: "Name",
                    controller: nameController,
                    textInputType: TextInputType.text,
                    autofocus: false,
                    validator: (v) {
                      if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 3) {
                        return "Please add a valid name";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 14,
                  ),
                  AppTextField(
                    autofocus: false,
                    hintText: "Phone Number",
                    textInputType: TextInputType.phone,
                    controller: phoneController,
                    validator: (v) {
                      if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 10) {
                        return "Please add a valid number";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  StatefulDropDown(
                    label: region,
                    items: [
                      DropdownMenuItem<String>(
                        value: "Lebanon",
                        child: Text(
                          'Lebanon',
                          style: const TextStyle(color: Colors.white),
                        ),
                        onTap: () {
                          region = 'Lebanon';
                        },
                      ),
                      DropdownMenuItem<String>(
                        value: "Coastal Region",
                        child: Text(
                          'Coastal Region',
                          style: const TextStyle(color: Colors.white),
                        ),
                        onTap: () {
                          region = 'Coastal Region';
                        },
                      ),
                      DropdownMenuItem<String>(
                        value: "Northern Region",
                        child: Text(
                          'Northern Region',
                          style: const TextStyle(color: Colors.white),
                        ),
                        onTap: () {
                          region = 'Northern Region';
                        },
                      ),
                      DropdownMenuItem<String>(
                        value: "Southern region",
                        child: Text(
                          'Southern region',
                          style: const TextStyle(color: Colors.white),
                        ),
                        onTap: () {
                          region = 'Southern region';
                        },
                      ),
                    ],
                  ),
                  Spacer(),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                    ),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    width: double.infinity,
                    child: MaterialButton(
                      color: Theme.of(context).colorScheme.primary,
                      onPressed: () async {
                        cubit.updateSalePerson(
                          name: nameController.text,
                          phone: phoneController.text,
                          region: region,
                          docId: widget.docId,
                          image: widget.salePersonModel.image,
                          email: widget.salePersonModel.email,
                          date: widget.salePersonModel.date,
                          coastalRegion: widget.salePersonModel.coastalRegion,
                          northernRegion: widget.salePersonModel.northernRegion,
                          lebanonRegion: widget.salePersonModel.lebanonRegion,
                          southernRegion: widget.salePersonModel.southernRegion,
                          easternRegion: widget.salePersonModel.easternRegion,
                          commission: widget.salePersonModel.commission,
                        );
                      },
                      child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          "Edit",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
