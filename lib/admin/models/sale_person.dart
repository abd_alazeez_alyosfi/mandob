import 'package:cloud_firestore/cloud_firestore.dart';

class SalePersonModel {
  final String? docId;
  final String image;
  final String name;
  final String region;
  final String phone;
  final String email;
  final Timestamp date;
  final num southernRegion;
  final num coastalRegion;
  final num northernRegion;
  final num easternRegion;
  final num lebanonRegion;
  final num commission;


  SalePersonModel(
      {this.docId,
      required this.phone,
      required this.image,
      required this.name,
      required this.region,
      required this.email,
        required this.date,
        required this.coastalRegion,
        required this.northernRegion,
        required this.lebanonRegion,
        required this.southernRegion,
        required this.easternRegion,
        required this.commission,
      });

  factory SalePersonModel.fromMap(Map<String, dynamic> data, [String? docId]) =>
      SalePersonModel(
        image: data['image'] ?? "",
        docId: docId ?? '',
        name: data['name'],
        region: data['region'],
        phone: data['phone'],
        email: data['email'],
        date: data['date'],
        coastalRegion: data['coastalRegion'],
        northernRegion: data['northernRegion'],
        lebanonRegion: data['lebanonRegion'],
        southernRegion: data['southernRegion'],
        easternRegion: data['easternRegion'],
        commission: data['commission'],
      );

  toMap() => {
        "name": name,
        "docId": docId,
        "image": image,
        "region": region,
        "phone": phone,
        "email": email,
        "date": date,
        "coastalRegion": coastalRegion,
        "northernRegion": northernRegion,
        "lebanonRegion": lebanonRegion,
        "southernRegion": southernRegion,
        "easternRegion": easternRegion,
        "commission": commission,
      };
}
