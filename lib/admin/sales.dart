import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mandob/admin/models/sale_person.dart';
import 'package:mandob/admin/new_sales_pesrson.dart';
import 'package:mandob/admin/sales_person_widget.dart';
import 'package:mandob/admin/satte/sales_cubit.dart';
import 'package:collection/collection.dart';
class SalesScreen extends StatefulWidget {
  const SalesScreen({Key? key}) : super(key: key);

  @override
  State<SalesScreen> createState() => _SalesScreenState();
}

class _SalesScreenState extends State<SalesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sales Persons',style: TextStyle(fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () async {
                FirebaseAuth.instance.signOut();
                Navigator.pop(context);
              },
              icon: Icon(Icons.logout))
        ],
      ),
      body:  BlocListener<SalesPersonCubit, SalesPersonState>(
  listener: (context, state) {
    if (state is SalesPersonLoading) {
      EasyLoading.show(status: 'Loading...');
    }
    {
      EasyLoading.dismiss();
    }
  },
  child: BlocBuilder<SalesPersonCubit, SalesPersonState>(
  builder: (context, state) {
    
    return Column(
      children: [
        Expanded(
          child: FutureBuilder(
            future: FirebaseFirestore.instance.collection('SalesPerson').get(),
            builder: (context,snapshot){
              if (snapshot.connectionState == ConnectionState.done) {
                final list = snapshot.data!.docs
                    .mapIndexed((i, e) =>
                    SalePersonModel.fromMap(e.data(), snapshot.data!.docs[i].id))
                    .toList();
                if (list.isEmpty) {
                  return const Center(
                      child: Text("There is no Sales Person yet"));
                }
                 return ListView.builder(
                   padding: const EdgeInsets.all(16),
                   itemBuilder: (context, index) {
                     return SalesPersonWidget(
                       salePersonModel: list[index],
                       docId: list[index].docId!,
                     );
                   },
                   itemCount: list.length,
                 );;
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return  Container();
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Colors.transparent,
            ),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            width: double.infinity,
            child: MaterialButton(
              color: Theme.of(context).colorScheme.primary,
              onPressed: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddSalesPerson()));
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Add Sales Person",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 24,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  },
),
)
    );
  }
}