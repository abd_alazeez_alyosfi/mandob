import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mandob/admin/edit_sales_person.dart';
import 'package:mandob/admin/models/sale_person.dart';
import 'package:mandob/admin/satte/sales_cubit.dart';
import 'package:mandob/user/commission_report.dart';

import '../shared/shared_widget.dart';
class SalesPersonWidget extends StatefulWidget {
  const SalesPersonWidget({super.key, required this.salePersonModel, required this.docId,this.fromSearch = false});
  final SalePersonModel salePersonModel;
  final String docId;
  final bool fromSearch;

  @override
  State<SalesPersonWidget> createState() => _SalesPersonWidgetState();
}

class _SalesPersonWidgetState extends State<SalesPersonWidget> {

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SalesPersonCubit, SalesPersonState>(
  listener: (context, state) {},
  builder: (context, state) {
    var cubit= SalesPersonCubit.get(context);
    return Container(
      padding: const EdgeInsets.all(8.0),
      margin: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          border: Border.all(color: Colors.deepPurple)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          circleAvatar(context,image: widget.salePersonModel.image,profileImage: null,onTap: () async{

          }),
          Text('${widget.salePersonModel.name}',textAlign: TextAlign.center,style: Theme.of(context).textTheme.titleMedium!.copyWith(color:Colors.deepPurple ,fontSize: 20)),
          Text('${widget.salePersonModel.region}',textAlign: TextAlign.center,),
          if(widget.fromSearch)
          Text('Monthly Commission : ${widget.salePersonModel.commission}',textAlign: TextAlign.center,),
          if(widget.fromSearch == false)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ElevatedButton(
                  onPressed:() async {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => EditSalesPerson(salePersonModel: widget.salePersonModel, docId: widget.docId,)));
                  },
                  child: Text('Edit')),
              ElevatedButton(
                  onPressed:() async {
                    cubit.deleteSalePerson(docId: widget.docId);
                  },
                  child: Text('Delete'),
              ),
            ],
          ),
        ],
      ),
    );
  },
);
  }
}
