import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart' hide me;
import 'package:image_picker/image_picker.dart';
import 'package:mandob/admin/models/sale_person.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import '../../shared/consatnts.dart';

part 'sales_state.dart';

class SalesPersonCubit extends Cubit<SalesPersonState> {
  SalesPersonCubit() : super(SalesPersonInitial());

  static SalesPersonCubit get(context) => BlocProvider.of(context);
  String profileImageUrl = '';

  void createSalePerson({
    required String name,
    required String phone,
    required String region,
    required String email,
  }) {
    emit(AddSalesPersonLoading());
    firebase_storage.FirebaseStorage.instance
        .ref()
        .child('SalesPerson/${Uri.file(profileImage!.path).pathSegments.last}')
        .putFile(profileImage!)
        .then((value) {
      print('iam here');
      value.ref.getDownloadURL().then((value) async {
        profileImageUrl = value;
        emit(SalesPersonLoading());
        Timestamp date = Timestamp.now();
        SalePersonModel salePersonModel = SalePersonModel(
          image: profileImageUrl,
          name: name,
          region: region,
          phone: phone,
          email: email,
          date: date,
          coastalRegion: 0,
          northernRegion: 0,
          lebanonRegion: 0,
          southernRegion: 0,
          easternRegion: 0,
          commission: 0,
        );
        await FirebaseFirestore.instance
            .collection('SalesPerson')
            .add(salePersonModel.toMap())
            .then((value) {
          emit(SalesPersonLoaded());
        }).catchError((onError) {
          emit(SalesPersonError());
        });
      }).catchError((onError) {
        emit(AddSalesPersonLoaded());
      });
    }).catchError((onError) {
      emit(AddSalesPersonError());
    });
  }

  void updateSalePerson({
    required String name,
    required String phone,
    required String region,
    required String docId,
    required String image,
    required String email,
    required Timestamp date,
    required num coastalRegion,
    required num northernRegion,
    required num lebanonRegion,
    required num southernRegion,
   required num easternRegion,
    required num commission,
  }) async {
    emit(UpdateSalesPersonLoading());

    SalePersonModel salePersonModel = SalePersonModel(
      image: image,
      name: name,
      region: region,
      phone: phone,
      docId: docId,
      email: email,
      date: date,
      coastalRegion: coastalRegion,
      northernRegion: northernRegion,
      lebanonRegion: lebanonRegion,
      southernRegion: southernRegion,
      easternRegion: easternRegion,
      commission: commission,
    );
    await FirebaseFirestore.instance
        .collection('SalesPerson')
        .doc(docId)
        .update(salePersonModel.toMap())
        .then((value) {
      emit(UpdateSalesPersonLoaded());
    }).catchError((onError) {
      emit(UpdateSalesPersonError());
    });
  }

  void deleteSalePerson({required String docId}) async {
    emit(DeleteSalesPersonLoading());
    await FirebaseFirestore.instance
        .collection('SalesPerson')
        .doc(docId)
        .delete()
        .then((value) {
      emit(DeleteSalesPersonLoaded());
    }).catchError((onError) {
      emit(DeleteSalesPersonError());
    });
  }
}

String get getUsrId =>
    FirebaseAuth.instance.currentUser?.uid ??
    Random().nextInt(100000000).toString();

String get getUserEmail => FirebaseAuth.instance.currentUser?.email ?? '';
