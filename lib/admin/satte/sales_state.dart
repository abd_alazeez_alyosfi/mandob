part of 'sales_cubit.dart';

@immutable
abstract class SalesPersonState {}

class SalesPersonInitial extends SalesPersonState {}
class SalesPersonLoading extends SalesPersonState {
}
class SalesPersonLoaded extends SalesPersonState {

}
class SalesPersonError extends SalesPersonState {

}
class UpdateSalesPersonLoading extends SalesPersonState {
}
class UpdateSalesPersonLoaded extends SalesPersonState {

}
class UpdateSalesPersonError extends SalesPersonState {

}
class DeleteSalesPersonLoading extends SalesPersonState {
}
class DeleteSalesPersonLoaded extends SalesPersonState {

}
class DeleteSalesPersonError extends SalesPersonState {

}
class AddSalesPersonLoading extends SalesPersonState {
}
class AddSalesPersonLoaded extends SalesPersonState {

}
class AddSalesPersonError extends SalesPersonState {

}
class ProfileImagePickerSuccessState extends SalesPersonState{}
class ProfileImagePickerErrorState extends SalesPersonState{}