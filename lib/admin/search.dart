import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mandob/admin/sales_person_widget.dart';

import '../shared/stateful_dropdown.dart';
import 'models/sale_person.dart';
import 'package:collection/collection.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  int year=2022;
  int month=11;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: const Text('Search Sales Persons',style: TextStyle(fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () async {
                FirebaseAuth.instance.signOut();
                Navigator.pop(context);
              },
              icon: Icon(Icons.logout))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            StatefulDropDown(
              label: "Select Year",
              items: [
                DropdownMenuItem<String>(
                  value: "2023",
                  child: Text(
                    '2023',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    year=2023;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "2022",
                  child: Text(
                    '2022',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    year=2022;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "2021",
                  child: Text(
                    '2021',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    year=2021;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "2020",
                  child: Text(
                    '2020',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    year=2020;
                    setState(() {

                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 10,),
            StatefulDropDown(
              label: "Select Month",
              items: [
                DropdownMenuItem<String>(
                  value: "January",
                  child: Text(
                    'January',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=1;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "February",
                  child: Text(
                    'February',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=2;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "March",
                  child: Text(
                    'March',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=3;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "April",
                  child: Text(
                    'April',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=4;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "May",
                  child: Text(
                    'May',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=5;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "June",
                  child: Text(
                    'June',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=6;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "July",
                  child: Text(
                    'July',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=7;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "August",
                  child: Text(
                    'August',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=8;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "september",
                  child: Text(
                    'september',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=9;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "October",
                  child: Text(
                    'October',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=10;
                    setState(() {
                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "November",
                  child: Text(
                    'November',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=11;
                    setState(() {

                    });
                  },
                ),
                DropdownMenuItem<String>(
                  value: "December",
                  child: Text(
                    'December',
                    style: const TextStyle(color: Colors.white),
                  ),
                  onTap: (){
                    month=12;
                    setState(() {

                    });
                  },
                ),
              ],
            ),
            // Expanded(
            //   child: ListView.builder(
            //     padding: const EdgeInsets.all(16),
            //     itemBuilder: (context, index) {
            //       return SalesPersonWidget();
            //     },
            //     itemCount: 6,
            //   ),
            // ),
            Expanded(
              child: FutureBuilder(
                future: FirebaseFirestore.instance.collection('SalesPerson').get(),
                builder: (context,snapshot){
                  if (snapshot.connectionState == ConnectionState.done) {
                    final list = snapshot.data!.docs
                        .mapIndexed((i, e) =>
                        SalePersonModel.fromMap(e.data(), snapshot.data!.docs[i].id))
                        .toList();
                    final filteredList = list.where(
                          (element) {

                        var data = element.date.toDate();
                        print(data.year.toString() + year.toString());
                        print(data.year == year);
                        return (data.year == year &&
                            data.month == month);
                      },
                    ).toList();
                    if (filteredList.isEmpty) {
                      return const Center(
                          child: Text("There is no Sales Person yet"));
                    }
                    return ListView.builder(
                      padding: const EdgeInsets.all(16),
                      itemBuilder: (context, index) {
                        return SalesPersonWidget(
                          salePersonModel: filteredList[index],
                          docId: filteredList[index].docId!,
                          fromSearch: true,
                        );
                      },
                      itemCount: filteredList.length,
                    );;
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return  Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
