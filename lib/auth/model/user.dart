class UserModel {
  final String email;

  final String id;

  UserModel({
    required this.email,
    required this.id,
  });

  factory UserModel.fromMap(Map<String, dynamic> data) => UserModel(
        id: data['id'],
        email: data['email'],
      );

  toMap() => {
        "id": id,
        "email": email,
      };
}

