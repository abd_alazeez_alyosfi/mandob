import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../model/user.dart';
import 'auth_state.dart';


class AuthCubit extends Cubit<AuthStates> {
  AuthCubit() : super(InitialAuthState());

  static AuthCubit get(context) => BlocProvider.of(context);
  bool isSecure = true;

  void changeSecure() {
    isSecure = !isSecure;
    emit(ChangeSecureAuthState());
  }

  Future<void> userLogin(
      {required String email,
      required String password,
}) async {
    try {
      emit(LoginLoadingAuthState());
      EasyLoading.show(status: 'Loading...');
      final data = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      var  isUser= FirebaseFirestore.instance
        .collection("users")
        .where("id", isEqualTo: getUserId);
      var userModel = UserModel(email: email, id: getUserId);
      await FirebaseFirestore.instance.collection("users").add(userModel.toMap());
       emit(LoginSuccessAuthState(userModel));
      EasyLoading.dismiss();
    } catch (e) {
      emit(LoginErrorAuthState(e.toString()));
      EasyLoading.dismiss();
    }
  }
}
String get getUserId =>
    FirebaseAuth.instance.currentUser?.uid ??
        Random().nextInt(100000000).toString();