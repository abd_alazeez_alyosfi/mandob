import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mandob/auth/state/auth_cubit.dart';
import 'package:mandob/auth/state/auth_state.dart';
import 'package:mandob/option%20Screen/option_screen.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthStates>(
      listener: (context, state) {
        if (state is LoginSuccessAuthState)
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => OptionScreen()));
      },
      builder: (context, state) {
        var cubit = AuthCubit.get(context);
        return Form(
          key: formKey,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Login',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              centerTitle: true,
            ),
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Center(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Login',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (c) {
                          if (!(c?.contains("@") ?? false)) {
                            return "Enter a valid email";
                          }
                          return null;
                        },
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 20,
                          ),
                          hintText: 'Email',
                          label: Text('Email'),
                          border: OutlineInputBorder(),
                          prefixIcon: Icon(Icons.email),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                        ),
                        onFieldSubmitted: (value) {
                          // print(value);
                        },
                        onChanged: (value) {
                          // print(value);
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (v) {
                          if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 6) {
                            return "Password are not true";
                          }
                          return null;
                        },
                        controller: passwordController,
                        keyboardType: TextInputType.text,
                        obscureText: cubit.isSecure ? true : false,
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 20),
                          hintText: 'Password',
                          label: const Text('Password'),
                          border: const OutlineInputBorder(),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              cubit.changeSecure();
                            },
                            icon: const Icon(Icons.remove_red_eye_outlined),
                          ),
                        ),
                        onFieldSubmitted: (value) {
                          //print(value);
                        },
                        onChanged: (value) {
                          // print(value);
                        },
                      ),
                      const SizedBox(
                        height: 59,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        width: double.infinity,
                        child: MaterialButton(
                          color: Theme.of(context).colorScheme.primary,
                          onPressed: () async {
                            AuthCubit.get(context).userLogin(
                                email: emailController.text,
                                password: passwordController.text);
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
