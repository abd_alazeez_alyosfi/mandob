import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextField extends StatelessWidget {
  const AppTextField({
    Key? key,
    this.controller,
    this.onTap,
    this.onEditingComplete,
    this.onChange,
    this.onFieldSubmitted,
    this.onSaved,
    this.maxLines,
    this.minLines,
    this.maxLength,
    this.enabled,
    this.textInputType,
    this.textInputAction,
    this.textDirection,
    this.validator,
    this.maxLengthEnforcement,
    this.focusNode,
    this.autoValidateMode,
    this.scrollPhysics,
    this.scrollController,
    this.initialValue,
    this.keyboardAppearance,
    this.textAlignVertical,
    this.obscuringCharacter = "•",
    this.expands = false,
    this.readOnly = false,
    this.autocorrect = true,
    this.showLength = false,
    this.filled = true,
    this.scrollPadding = const EdgeInsets.all(20.0),
    this.textAlign = TextAlign.start,
    this.textCapitalization = TextCapitalization.none,
    this.titleField,
    this.obscure = false,
    this.prefixIcon,
    this.icon,
    this.hintTextStyle,
    this.textStyle,
    this.suffixIcon,
    this.suffix,
    this.hintText,
    this.labelText,
    this.inputFormatters,
    this.contentPadding,
    this.borderColor,
    this.prefix,
    this.autofocus, this.prefixText, this.height,
  }) : super(key: key);
final double? height ;
  final TextEditingController? controller;
  final bool? autofocus;
  final void Function()? onTap;
  final void Function()? onEditingComplete;
  final void Function(String val)? onChange;
  final void Function(String val)? onFieldSubmitted;
  final void Function(String? val)? onSaved;
  final int? maxLines;
  final int? minLines;
  final int? maxLength;
  final bool? enabled;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;
  final TextDirection? textDirection;
  final FormFieldValidator<String?>? validator;
  final MaxLengthEnforcement? maxLengthEnforcement;
  final FocusNode? focusNode;
  final AutovalidateMode? autoValidateMode;
  final ScrollPhysics? scrollPhysics;
  final ScrollController? scrollController;
  final String? initialValue;
  final Brightness? keyboardAppearance;
  final TextAlignVertical? textAlignVertical;
  final TextCapitalization textCapitalization;
  final TextAlign textAlign;
  final EdgeInsets scrollPadding;
  final bool expands;
  final bool readOnly;
  final bool autocorrect;
  final String obscuringCharacter;
  final String? titleField;
  final bool showLength;
  final bool filled;
  final bool obscure;
  final Widget? prefixIcon;
  final Widget? icon;
  final Widget? suffixIcon;
  final Widget? suffix;
  final Widget? prefix;
  final String? hintText;
  final TextStyle? hintTextStyle;
  final TextStyle? textStyle;
  final String? labelText;
  final List<TextInputFormatter>? inputFormatters;
  final EdgeInsetsGeometry? contentPadding;
  final Color? borderColor;
final String?  prefixText ;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height:height ??  70,
      child: TextFormField(
        autofocus: autofocus ?? false,
        controller: controller,
        inputFormatters: inputFormatters,
        onTap: onTap,
        onChanged: onChange,
        onFieldSubmitted: onFieldSubmitted,
        onEditingComplete: onEditingComplete,
        onSaved: onSaved,
        validator: validator,
        maxLines: obscure ? 1 : maxLines,
        minLines: minLines,
        // maxLength: showLength ? maxLength : null,
        textAlign: textAlign,

        enabled: enabled,
        keyboardType: textInputType,
        textInputAction: textInputAction,
        textDirection: textDirection,
        scrollPadding: scrollPadding,
        expands: expands,
        maxLengthEnforcement: maxLengthEnforcement,
        focusNode: focusNode,
        obscureText: obscure,
        obscuringCharacter: obscuringCharacter,
        autovalidateMode: autoValidateMode,
        readOnly: readOnly,
        scrollPhysics: scrollPhysics,
        scrollController: scrollController,
        autocorrect: false,
        keyboardAppearance: keyboardAppearance,

        textAlignVertical: textAlignVertical,
        textCapitalization: textCapitalization,

        decoration: InputDecoration(hintText: hintText, suffixIcon:prefixText != null ? Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Text(prefixText!,style: Theme.of(context).textTheme.titleMedium
                ?.copyWith(color: Theme.of(context).colorScheme.onSurface)
            ),
          ),
        ):null,suffixIconConstraints: const BoxConstraints(maxWidth: 55)  ),
      ),
    );
  }
}
