import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


Widget circleAvatar(context,{required var image, required var profileImage,bool isEdit = false,Function()? onTap}) {
  return Stack(
    alignment: Alignment.center,
    children: [
      ClipOval(
        child: Opacity(
          opacity: 0.1,
          child: Container(
            color: Colors.grey,
            height: 100,
            width: 100,
          ),
        ),
      ),
      ClipOval(
        child: Container(
          color: Colors.white,
          width: 90,
          height: 90,
        ),
      ),
      ClipOval(
        child: Container(
          height: 80,
          width: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(4),
              topLeft: Radius.circular(4),
            ),
            image: DecorationImage(
              image: image != null ?  NetworkImage(
                image,
              ): profileImage == null
                  ? NetworkImage(
                'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png',
              )
                  : FileImage(profileImage!) as ImageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      isEdit ?   Positioned(
        bottom: 5,
        right: MediaQuery.of(context).size.width * 0.4,
        child: ClipOval(
          child: Container(
            color: Colors.blue,
            child: IconButton(
              onPressed: onTap,
              color: Colors.white,
              icon: Icon(Icons.camera_alt,size: 14,),
            ),
            width: 30,
            height: 30,
          ),
        ),
      ):Container(),
    ],
  );
}