
import 'package:flutter/material.dart';


class StatefulDropDown extends StatefulWidget {
  const StatefulDropDown({Key? key, this.items, this.initialValue,this.label, this.isDialog=false})
      : super(key: key);
  final Object? initialValue;
  final String? label;
  final List<DropdownMenuItem<Object>>? items;
  final bool isDialog;
  @override
  State<StatefulDropDown> createState() => _StatefulDropDownState();
}

class _StatefulDropDownState extends State<StatefulDropDown> {
  Object? selectedValue;

  @override
  void initState() {
    selectedValue = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:  widget.isDialog ? EdgeInsets.symmetric(horizontal: 0.0) :  EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.deepPurple,
        borderRadius: BorderRadius.circular(10)
      ),
      child: DropdownButton(
          isExpanded: true,
          underline: const SizedBox(),
          itemHeight: 55,
          hint: widget.label != null ? Text(widget.label!,style: TextStyle(color: Colors.white),) : null,
          items: widget.items,
          value: selectedValue,
          iconEnabledColor: Colors.grey,
          dropdownColor: Colors.deepPurple,
          borderRadius: BorderRadius.circular(30),
          onChanged: (value) {
            if(value != null){
              setState(() {
                selectedValue = value;
              });
            }
          }),
    );
  }
}
