import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';

import '../admin/models/sale_person.dart';
import '../admin/satte/sales_cubit.dart';

class CommissionReport extends StatefulWidget {
  const CommissionReport({Key? key}) : super(key: key);

  @override
  State<CommissionReport> createState() => _CommissionReportState();
}

class _CommissionReportState extends State<CommissionReport> {
  @override
  Widget build(BuildContext context) {
  return  FirestoreQueryBuilder(
      query: FirebaseFirestore.instance
          .collection("SalesPerson")
          .where("email", isEqualTo: getUserEmail),
      builder: (context, snapshot, child) {
        if (snapshot.isFetching) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.hasData) {
          final salePerson = SalePersonModel.fromMap(snapshot.docs.first.data());
          return Scaffold(
            appBar: AppBar(
              title: const Text('Commission Report',style: TextStyle(fontWeight: FontWeight.bold)),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Salesperson Name: ${salePerson.name}"),
                    SizedBox(height: 20,),
                    Text("Registration Date ${salePerson.date.toDate().year}/${salePerson.date.toDate().month}/${salePerson.date.toDate().day}"),
                    SizedBox(height: 20,),
                    Text("Southern region: ${salePerson.southernRegion}SYP"),
                    SizedBox(height: 20,),
                    Text("Coastal region: ${salePerson.coastalRegion}SYP"),
                    SizedBox(height: 20,),
                    Text("Northern Region: ${salePerson.northernRegion}SYP"),
                    SizedBox(height: 20,),
                    Text("Eastern Region: ${salePerson.easternRegion}SYP"),
                    SizedBox(height: 20,),
                    Text("Lebanon: ${salePerson.lebanonRegion}SYP"),
                    SizedBox(height: 20,),
                    Text("Monthly commission: ${salePerson.commission}SYP"),
                  ],
                ),
              ),
            ),
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
