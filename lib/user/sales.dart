import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mandob/admin/satte/sales_cubit.dart';
import 'package:mandob/user/commission_report.dart';
import 'package:flutter/services.dart';
import '../admin/models/sale_person.dart';
import '../shared/app_text_field.dart';
import '../shared/shared_widget.dart';
import '../shared/stateful_dropdown.dart';

class UserSalesScreen extends StatefulWidget {
  const UserSalesScreen({Key? key}) : super(key: key);

  @override
  State<UserSalesScreen> createState() => _UserSalesScreenState();
}

class _UserSalesScreenState extends State<UserSalesScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController regionController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  String region = '';
  num amount=0;
  num commission=0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sales Persons',
            style: TextStyle(fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () async {
                FirebaseAuth.instance.signOut();
                Navigator.pop(context);
              },
              icon: Icon(Icons.logout))
        ],
      ),
      body: FirestoreQueryBuilder(
        query: FirebaseFirestore.instance
            .collection("SalesPerson")
            .where("email", isEqualTo: getUserEmail),
        builder: (context, snapshot, child) {
          if (snapshot.isFetching) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasData) {
            final salePerson =
                SalePersonModel.fromMap(snapshot.docs.first.data());
            nameController.text = salePerson.name;
            regionController.text = salePerson.region;
            return Padding(
              padding: const EdgeInsets.all(16),
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(
                          height: 36,
                        ),
                        circleAvatar(context,
                            image: salePerson.image,
                            profileImage: null,
                            onTap: () async {}),
                        AppTextField(
                          readOnly: true,
                          hintText: "Name",
                          controller: nameController,
                          textInputType: TextInputType.text,
                          autofocus: false,
                          validator: (v) {
                            if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 3) {
                              return "Please add a valid name";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 14,
                        ),
                        AppTextField(
                          readOnly: true,
                          autofocus: false,
                          hintText: "Main Region",
                          textInputType: TextInputType.text,
                          controller: regionController,
                          validator: (v) {
                            if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 10) {
                              return "Please add a valid region";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Divider(
                          color: Colors.deepPurple,
                        ),
                        AppTextField(
                          autofocus: false,
                          hintText: "Add Sale Quantity",
                          textInputType: TextInputType.numberWithOptions(decimal: true),
                          controller: quantityController,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d{0,2}'))
                          ],                          validator: (v) {
                            if ((v?.isEmpty ?? true) || (v?.length ?? 0) < 15) {
                              return "Please add a valid number";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        StatefulDropDown(
                          label: "Select Sales Area",
                          items: [
                            DropdownMenuItem<String>(
                              value: "Lebanon",
                              child: Text(
                                'Lebanon',
                                style: const TextStyle(color: Colors.white),
                              ),
                              onTap: () {
                                region = 'Lebanon';
                              },
                            ),
                            DropdownMenuItem<String>(
                              value: "Coastal Region",
                              child: Text(
                                'Coastal Region',
                                style: const TextStyle(color: Colors.white),
                              ),
                              onTap: () {
                                region = 'Coastal Region';
                              },
                            ),
                            DropdownMenuItem<String>(
                              value: "Eastern Region",
                              child: Text(
                                'Eastern Region',
                                style: const TextStyle(color: Colors.white),
                              ),
                              onTap: () {
                                region = 'Eastern Region';
                              },
                            ),
                            DropdownMenuItem<String>(
                              value: "Northern Region",
                              child: Text(
                                'Northern Region',
                                style: const TextStyle(color: Colors.white),
                              ),
                              onTap: () {
                                region = 'Northern Region';
                              },
                            ),
                            DropdownMenuItem<String>(
                              value: "Southern region",
                              child: Text(
                                'Southern region',
                                style: const TextStyle(color: Colors.white),
                              ),
                              onTap: () {
                                region = 'Southern region';
                              },
                            ),
                          ],
                        ),
                        //Spacer()
                        SizedBox(height: 200,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                          ),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          width: double.infinity,
                          child: MaterialButton(
                            color: Theme.of(context).colorScheme.primary,
                            onPressed: () async {
                              print('region${region == salePerson.region}');
                              if (region == 'Lebanon') {
                                if (region == salePerson.region) {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.5;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.7;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                } else {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.3;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.4;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion + amount,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                }
                              } else if (region == 'Coastal Region') {
                                if (region == salePerson.region) {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.5;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion +amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.7;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion + amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion + amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                } else {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.3;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion:
                                              salePerson.coastalRegion + amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.4;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion:
                                              salePerson.coastalRegion + amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion:
                                              salePerson.coastalRegion + amount,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                }
                              } else if (region == 'Northern Region') {
                                if (region == salePerson.region) {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.5;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.7;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                } else {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.3;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion:
                                              salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.4;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion + amount,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                }
                              } else if (region == 'Southern region') {
                                if (region == salePerson.region) {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.5;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.7;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                } else {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.3;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.4;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion + amount,
                                          easternRegion: salePerson.easternRegion,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                }
                              } else if (region == 'Eastern Region') {
                                if (region == salePerson.region) {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.5;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion:
                                              salePerson.easternRegion + amount,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.7;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion:
                                              salePerson.easternRegion + amount,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion:
                                              salePerson.easternRegion + amount,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                } else {
                                  amount = num.parse(quantityController.text);
                                  if (amount == 1000000) {
                                    commission = amount * 0.3;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion:
                                              salePerson.easternRegion + amount,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else if (amount > 1000000) {
                                    commission = amount * 0.4;
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion: salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion:
                                              salePerson.easternRegion + amount,
                                          commission:
                                              salePerson.commission + commission,
                                        ).toMap());
                                  } else {
                                    await FirebaseFirestore.instance
                                        .collection("SalesPerson")
                                        .doc(snapshot.docs.first.id)
                                        .update(SalePersonModel(
                                          image: salePerson.image,
                                          name: salePerson.name,
                                          region: salePerson.region,
                                          phone: salePerson.phone,
                                          docId: salePerson.docId,
                                          email: salePerson.email,
                                          date: salePerson.date,
                                          coastalRegion: salePerson.coastalRegion,
                                          northernRegion:
                                              salePerson.northernRegion,
                                          lebanonRegion:
                                              salePerson.lebanonRegion,
                                          southernRegion:
                                              salePerson.southernRegion,
                                          easternRegion: salePerson.easternRegion +amount,
                                          commission: salePerson.commission + 0,
                                        ).toMap());
                                  }
                                }
                              }
                            },
                            child: const Padding(
                              padding: EdgeInsets.symmetric(vertical: 8.0),
                              child: Text(
                                "Add Selling Amount",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                          ),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          width: double.infinity,
                          child: MaterialButton(
                            color: Theme.of(context).colorScheme.primary,
                            onPressed: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CommissionReport()));
                            },
                            child: const Padding(
                              padding: EdgeInsets.symmetric(vertical: 8.0),
                              child: Text(
                                "Commission Calculation",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}
